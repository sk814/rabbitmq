# Steps to run

1. Clone the repository
2. Checkout to `dev` branch
3. Build order service image (example are for linux)
    `cd orders`
    `sudo docker build -t orders:latest .`
4. Start all the services using docker-compose.yml
    `sudo docker-compose up --build`

5. Run migration inside running service container
    `sudo docker exec -it order-service sh`
    `cd orders`
    `python3 manage.py makemigrations`
    `python3 manage.py migrate`

6. Services available on port 8001
    `http://0.0.0.0:8001/api/order`

## Use Cases results

#### Create Order #1
```json
{
    "user_id": "7c11e1ce2741",
    "product_code": "classic-box"
}
```
#### Response
HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept
```json
{
    "data": {
        "user_id": "7c11e1ce2741",
        "product_code": "classic-box",
        "customer_fullname": "Ada Lovelace",
        "product_name": "Classic Box",
        "total_amount": 9.99
    }
}
```

#### Create Order #2
```json
{
    "user_id": "7c11e1ce2741",
    "product_code": "family-box"
}
```
#### Response
(Delay of 60 sec on "family-box")

HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept
```json
{
    "data": {
        "user_id": "7c11e1ce2741",
        "product_code": "family-box",
        "customer_fullname": "Ada Lovelace",
        "product_name": "Family Box",
        "total_amount": 14.99
    }
}
```


#### Create Order #3
```json
{
    "user_id": "7c11e1ce2741",
    "product_code": "veggie-box"
}
```
#### Response
(Fail after retrying 5 times)

HTTP 500 Internal Server Error
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept
```json
{
    "message": "Unable to process order",
    "error": "Max retries 5 Exceeded, Getting response Not able to get response from http://product-service:8080/products/veggie-box, Received Code 500, Response b''"
}
```

#### Create Order #4
```json
{
    "user_id": "e6f24d7d1c7e",
    "product_code": "classic-box"
}
```
#### Response
(Successed after retrying secound time for user e6f24d7d1c7e)

HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept
```json
{
    "data": {
        "user_id": "e6f24d7d1c7e",
        "product_code": "classic-box",
        "customer_fullname": "Alan Turing",
        "product_name": "Classic Box",
        "total_amount": 9.99
    }
}
```

#### Create Order #5
```json
{
    "user_id": "unknown",
    "product_code": "family-box"
}
```
#### Response
(Fail with unknown user)

HTTP 500 Internal Server Error
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept
```json
{
    "message": "Unable to process order",
    "error": "User for ID unknown not found"
}
```

## Sample Run
<p align="center">
  <img src="docs/order-service.png" alt="sample run" />
</p>

## Running container
<p align="center">
  <img src="docs/containers.png" alt="sample run" />
</p>

## Ordermicroservice flow
<p align="center">
  <img src="docs/order_service_flow.png" alt="order flow" />
</p>

## Async call of product and user endpoints
<p align="center">
  <img src="docs/asyn_call_of_endpoints_logs.png" alt="parallel call" />
</p>
